// Reflect methods are same methods with Proxy handles
// not a function object so it's not consntructible
// all properties and methods of Reflect are static 
// (Just like the `Math` Object)

// See the static Methods
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Reflect

const duck = {
    name: 'Maurice',
    color: 'white',
    greeting: function() {
      console.log(`Quaaaack! My name is ${this.name}`)
    }
  }
  
 let hasColor = Reflect.has(duck, 'color')
  // true
let hasHaircut = Reflect.has(duck, 'haircut')
  // false

  console.log(hasColor)
  console.log(hasHaircut)

const objKeys = Reflect.ownKeys(duck)
console.log(objKeys)

const setEyes = Reflect.set(duck, 'eyes', 'black')
// returns "true" if successful
// "duck" now contains the property "eyes: 'black'"
console.log(setEyes)
console.log(duck)