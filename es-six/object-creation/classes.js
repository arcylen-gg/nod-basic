// Javascript does not have classes
// class keyword is a syntactic sugar on the top of prototype model
// -- theres no modifiers in javascript (private, public, protected)
// -- no tricks to do it either
// -- but some are using _ to indicate that a property or method is private
// -- classes in Javascript are functions

// WHY Javascript don't have modifiers
// Because javascript does NOT have classes 

class Mammal {
    constructor(sound) {
        this.sound = sound
    }
    talk() {
       return this.sound
    }
}
class Cat extends Mammal {
    constructor() {
        super('adssadsadasdasd') // call the parent constructor
    }
}

let aigoo =  new Cat();
aigoo.sound = 'meowws'
// console.log(aigoo.talk());
console.log(typeof Cat) // returns a Functions 

// bind the object, creates new functions for talk
let x = Cat.prototype.talk.bind({
    sound : 'Woof'
})()
console.log(x)