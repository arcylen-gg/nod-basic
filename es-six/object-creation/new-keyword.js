/*
function Person(saying) {
    this.saying = saying
}

Person.prototype.talk = function() {
    console.log('I say: ', this.saying);
}

var arcy = new Person('Cool!');
arcy.talk();
*/

// WHY
// -- you are going to run across this
// -- class keyword uses this technique 
// this under or behind the scene
// -- in order to understand the present we need to understand 
// the history (how things were added)
// -- it's there and understand the javascript fully

// WHAT new does?
// -- creates new object (No nothing)
// -- look what we ever we called on (Set the prototype)
// -- constructor, call it and assigned it to `this` (execute the constructor with `this`)
// -- returned the new object created

function Person(saying) {
    this.saying = saying
}

Person.prototype.talk = function() {
    console.log('I say: ', this.saying);
}
// How new works! in ES5
function newSample(constructor) {
    // 1
    var obj = {}
    // 2
    Object.setPrototypeOf(obj, constructor.prototype)
    // var argsArr = Array.from(arguments);
    var argsArr = Array.prototype.slice.apply(arguments); // Same function of Array.from but old form
    constructor.apply(obj, argsArr.slice(1))
    return obj
}

var arcy = newSample(Person, 'Cool!');
arcy.talk();