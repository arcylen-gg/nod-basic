// Creates a `new object` with the `prototype` set to a certain object
const cat = {
    makeSound: function() {
        console.log(this.sound)
    }
}
// the cat is the prototype of mark or the created new object
// is it like Building [Builder in Design Pattern] an Object? [YESSS] 
// mutate the object to a new object? [I don't think so]
const mark = Object.create(cat)
mark.sound = 'meeeoooowww'
mark.makeSound();

const waffles = Object.create(cat)
waffles.sound = 'mrroooowwww'
waffles.makeSound()

// WHYYY Object.create
// more natural to the prototype model than the new keyword
// built in to javascript 
// jumbles together the creation of the object and setting prototype but
// setPrototypeOf and messing prototype directly on existing
// object bad idea for performance (Optimization)
// never use setPrototypeOf in RealLife [Nice for explaining] 

// How to use Object.create
function objectCreate(proto) {
    const obj = {}; // create new object
    Object.setPrototypeOf(obj, proto) // set prototype to object
    return obj //return the object
}

const markOC = objectCreate(cat)
markOC.sound = 'meeeoooowww'
markOC.makeSound();

const wafflesOC = objectCreate(cat)
wafflesOC.sound = 'mrroooowwww'
wafflesOC.makeSound()

