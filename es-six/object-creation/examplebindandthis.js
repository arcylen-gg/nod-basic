
 // EXAMPLE OF `bind` and `this`

 // -- `this` special keyword that you can use in the function
 // -- exactly the same thing that it does in english
 // -- `this` is refers to the global object
 let talk = function() {
    console.log(this.sound);
 }

let hermoine = { // Object 
   sound: 'Wingardium leviosa!', // Property
}
hermoine.speak = talk.bind(hermoine);
let blabber = hermoine.speak;
blabber();
// NOTE: function are just values that you can pass around 
// (as Property, argument, object and etc)

// hermoine variable is binded to talk function so it gets
// the sound property
// bind is a way to be extremely explicit by we mean by this
// let talkBoundToHermoine = talk.bind(hermoine); 
// talkBoundToHermoine();
// talk();

// hermoine.speak();
// talk();