// SYMBOLS
// New datatype/primitive type in ES6
// does not support `new` keyword
// debugging purposes

let symbol1 = Symbol('symbol')
let symbol2 = Symbol('symbol')

// symbols are unique no matter what
console.log(symbol1 === symbol2) // return false


// USE CASE 1
const myKey = Symbol()
let obj = {}

obj[myKey] = 123
console.log(obj[myKey])

// Use as keys to object
// it's not innumerable (too many to counted)
// obj.
// You can make your own object or class iterable by using symbols
// How ?

Number()
String()
Boolean()

Symbol()

// WHY
// use them as identifier to an object properties
/**
let pet = {
    id : 123,
    name : 'Aigoo',
    species : 'cat',
    age : 2
}
const idSym = Symbol('id')
pet[idSym] = 4324324
*/
const idSym = Symbol('id')
let pet = {
    id : 123,
    name : 'Aigoo',
    species : 'cat',
    age : 2,
    [idSym] : 134324 // key should be defined/declared as symbol first
}
console.log(pet)
console.log(Object.getOwnPropertyNames(pet)) // Symbol was hidden, even in foreach
// Not private, but you can access symbol, they are not just obvious
console.log(Object.getOwnPropertySymbols(pet))

// .for Method, can reference between iframes or to share symbols
const symCat = Symbol.for('cat')
const symCat2 = Symbol.for('cat')
console.log(symCat === symCat2)

// To create unique id's to we can add something in totally unique keys
// no conflict, not threat of overriding things