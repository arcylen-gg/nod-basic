// Object literals can be associate with encapsulation
let userOne = { // Object
    email: 'arcy@gmail.com', // Property
    name: 'Arcy Gutierrez', // Property
    login(){ // Method
        console.log(this.email, ' has logged in')
    },
    logout(){ // Method
        console.log(this.email, ' has logged out')
    }
}
console.log(userOne)

// ENHANCED OBJECT LITERALS
// extended to support setting the prototype at construction
// Together, these also bring object literals and
// class declarations closer together