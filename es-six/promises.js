// PROMISES
// Callback serves as same as Promises
// Promises are composable
// Promise have method `then` that serves as 
// the callback function for the promise

// Promise constructor takes single functions as arguments but this fucntion
// have 2 arguments, `resolve` & `reject`
// resolve -- success value [Callbacks]
// reject -- error/failure value [Callbacks]

let promise = new Promise((resolve, reject) => {
    let obj = { name : 'Aigoo' }
    resolve(obj)
    reject('Error!')
})

promise.then((objPromise) => {
    console.log(objPromise)
})

// Commit to do something, it's either to resolve or rejected the promise

let p = new Promise((resolve, reject) => {
    let a = 1 + 2
    if (a == 2) resolve('Success')
    reject('Failed')
})
p.then((messageResolve) => { // method if Resolve
    console.log(messageResolve)
}).catch((messageReject) => { // method if Rejected
    console.log(messageReject)
})

// PROMISE ALL

const recordVideoOne = new Promise((resolve, reject) => {
    resolve('Video 1 Record')
})
const recordVideoTwo = new Promise((resolve, reject) => {
    resolve('Video 2 Record')
})
const recordVideoThree = new Promise((resolve, reject) => {
    resolve('Video 3 Record')
})
// Array of promises
Promise.all([
    recordVideoOne,
    recordVideoTwo,
    recordVideoThree
]).then((messages) => {
    console.log(messages)
})

// same with the `Promise.all` but `Promise.race`
// will resolve only the first promise to be resolved

// ES6 came with many new features, but one of the best features
// was the official introduction of Promises. Promises allow you to
// write clean non-callback-centric code without ever having to worry
// about callback hell. Even if you never write your own promise, knowing
// how they work is incredibly important, since many newer parts of the
// JavaScript API use promises instead of callbacks. 