// Math + Number + String + Array + Object APIs

//math.pow(base, exponent)
console.log(Math.pow(3, 4)) // 3 * 3 * 3 * 3 = 81

console.log(Number.EPSILON)
console.log(Number.isInteger(Infinity)) // false
console.log(Number.isNaN("NaN")) // false

console.log(Math.acosh(3)) // 1.762747174039086
console.log(Math.hypot(3, 4)) // 5
console.log(Math.imul(Math.pow(2, 32) - 1, Math.pow(2, 32) - 2)) // 2

console.log("abcde".includes("cd")) // true
console.log("abc".repeat(3)) // "abcabcabc"

// Array.from(document.querySelectorAll('*')) // Returns a real Array
console.log(Array.of(1, 2, 3)) // Similar to new Array(...), but without special one-arg behavior
let arrFill = [0, 0, 0]
console.log(arrFill.fill(7, 1)) // [0,7,7]
let arrFind = [1, 2, 3]
console.log(arrFind.find(x => x == 3)) // 3
console.log(arrFind.findIndex(x => x == 2)) // 1
let arrCopyWith = [1, 2, 3, 4, 5]
console.log(arrCopyWith.copyWithin(3, 0)) // [1, 2, 3, 1, 2]
let arrIterator = ["a", "b", "c"]
console.log(arrIterator.entries()) // iterator [0, "a"], [1,"b"], [2,"c"]
console.log(arrIterator.keys()) // iterator 0, 1, 2
console.log(arrIterator.values()) // iterator "a", "b", "c"

//The Object.assign() method copies all enumerable own properties
// from one or more source objects to a target object. It returns 
// the target object.
const target = { a: 1, b: 2 };
const source = { b: 4, c: 5 };

const returnedTarget = Object.assign(target, source);

console.log(target);
// expected output: Object { a: 1, b: 4, c: 5 }

console.log(returnedTarget);
// expected output: Object { a: 1, b: 4, c: 5 }

