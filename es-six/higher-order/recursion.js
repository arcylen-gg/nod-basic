// Recursion
// When the function called itself until it doesn't

let countDownFrom = (num) => {

    console.log(num)
    return num === 1 ? num : countDownFrom(num - 1)
}

countDownFrom(10)

// maximum call stack size exceeded removed in ES6
// tail coal optimization

// WHY

let categories = [
    { id: 'animals', 'parent': null },
    { id: 'mammals', 'parent': 'animals' },
    { id: 'cats', 'parent': 'mammals' },
    { id: 'dogs', 'parent': 'mammals' },
    { id: 'chihuahua', 'parent': 'dogs' },
    { id: 'aspin', 'parent': 'dogs' },
    { id: 'persian', 'parent': 'cats' },
    { id: 'puspin', 'parent': 'cats' }
]

let animalTree = (categories, parent = null) => {
    let node = {}
    categories
        .filter(x => x.parent == parent)
        .forEach(x => 
            node[x.id] = animalTree(categories, x.id))
    return node
}

console.log(JSON.stringify(animalTree(categories), null, 2))


// think about your outputs
/* {
"animals": {
    "mamals": {
        "cats": {
        "persian": {},
        "puspin": {}
        },
        "dogs": {
        "chihuahua": {},
        "aspin": {}
        }
    }
} */