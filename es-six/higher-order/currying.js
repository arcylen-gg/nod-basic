// Currying
// is when a function doesn't take all of its arguments upfront instead
// its wants you to give the first arguments and then the function returns
// an another function which ypu are supposed to call with second arguments


let pet = (name, species, food) => 
 `${name} is ${species} that eats ${food} !`

// lodash library will convert the `pet` variable
// to be in a currable functions
// import _ from lodash
// pet = _.curry(pet)

console.log(pet('Aigoo', 'cat', 'boiled fish'))


// It written from the start to be curable
let pet2 = 
    name =>
        species =>
            food => 
                `${name} is ${species} that eats ${food} !`

                // Chain of a function
console.log(pet2('Hamtaro')('dog')('bones'))

let petName = pet2('Chukoy')
let petSpecies = petName('dog')
let petFood = petSpecies('human poop')
console.log(petFood)

/// Second example
let pets = [
    {name: 'Aigoo', power: 'ice'},
    {name: 'Cornietta', power: 'motherhood'},
    {name: 'Hamtaro', power: 'water'},
    {name: 'Chukoy', power: 'earth'},
    {name: 'Mong mong', power: 'earth'}
]

let hasPower = (power, obj) => obj.power === power

// lodash
// let hasPower = _.curry((power, obj) => obj.power === power)
// let earthPets = 
//     pets.filter(hasPower('earth'))
// end lodash

let earthPets = 
    pets.filter(x => hasPower('earth', x))

console.log(earthPets)

// Conclusion
// a currable functions is simpy functions that takes every arguments
// by itself and then just returns a new functions that the next dependecy
// to the fucntion until dependencies have been 
// fulfilled and the final value is returned