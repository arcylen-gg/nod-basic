class Heap {
  constructor(selector) {
    this.items = [];
    this.selector = selector;
  }

  insert() {
    let i = this.items.length;
    let parentI = Math.floor((i+1)/2 - 1);
    if (parentI < 0) parentI = 0;
    let pVal = this.selector(this.items[parentI]);
    const pushedV = this.selector(this.items[i]);
    while (i > 0 && pVal > pushedV) {
        parentI = Math.floor((i+1)/ 2-1);
        this.swap(i, parentI);
        i = parentI;
        pVal = this.selector(this.items[Math.max(Math.floor((i+1)/2 -1), 0)]);
    }
  }
  extract() {
    if (this.items.length <= 1) return this.items.pop();
    const ret = this.items[0];
    let temp = this.items.pop();
    this.items[0] = temp;

    let i = 0;
    while (true) {
      let rChildI = (i+1) * 2;
      let lChildI = (i+1) * 2 - 1;
      let lowest = rChildI;
      if (lChildI >= this.items.length &&
      rChildI >= this.items.length) {
        // break;
        if (lChildI >= this.items.length) lowest = rChildI;
        if (rChildI >= this.items.length) lowest = lChildI;
        if(!(lChildI >= this.items.length) && !(rChildI >= this.items.length)) {
          lowest = this.selector(this.items[rChildI]) <
          this.selector(this.items[lChildI]) ? rChildI : lChildI;
        }
        if(this.selector(this.items[i]) > this.selector(this.items[lowest])) {
          this.swap(i, lowest);
          i = lowest
        } else break;
      }
      return ret;
    }
  }
}

function ArrayChallenge(strArr) { 

  let newArr = [];
  // code goes here  
  for (i = 0; i < strArr.length; i++) {
    
  }
  return strArr; 

}
   
// keep this function call here 
console.log(ArrayChallenge(readline()));