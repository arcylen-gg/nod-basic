/* const robot1 = {
    name: 'Tom',
    weight: 30,
    color: 'Red',
    introduceSelf() {
        console.log(`Hello I'm ${this.name}`)
    }
}

const robot2 = {
    name: 'Jerry',
    weight: 40,
    color: 'Blue',
    introduceSelf() {
        console.log(`Hello I'm ${this.name}`)
    }
}

robot1.introduceSelf()
robot2.introduceSelf()

*/
class Robot {
    constructor(data) {
        this.name = data.name
        this.weight = data.weight
        this.color = data.color
    }
    introduceSelf() {
        console.log(`Hello I'm ${this.name}`)    
    }
}
const robot1Data = {
    name: 'Tom',
    weight: 30,
    color: 'Red'
}

const robot1 = new Robot(robot1Data)


const robot2Data = {
    name: 'Tom',
    weight: 30,
    color: 'Red'
}

const robot2 = new Robot(robot2Data)

robot1.introduceSelf()
robot2.introduceSelf()
console.log(robot1)
console.log(robot2)