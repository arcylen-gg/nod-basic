/*
    1. Call stact Execution - stack overflow (an error associated with
        the operations of the call stack)
*/
/*
    2. Primitive Data Types

    length property on `string`
    This feature is called autoboxing. In the above example, 
    JavaScript wraps the constant in a temporary wrapper object and
    then accesses the length property of that object. Once this step 
    is complete, the object is safely discarded.

*/
