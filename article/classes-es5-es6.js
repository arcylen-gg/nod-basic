// Please stop using Classes in Javascript and become a better developer
// https://medium.com/javascript-in-plain-english/please-stop-using-classes-in-javascript-and-become-a-better-developer-a185c9fbede1


// -- ES5
// "class" declaration 
/*
function Car(make, model) {
    this.make = make;
    this.model = model;
  }
  
  // the start method
  Car.prototype.start = function() {
    console.log('vroom');
  }
  
  // overriding the toString method
  Car.prototype.toString = function() {
    console.log('Car - ' + this.make + ' - ' + this.model);
  }
  
  // inheritance example
  function SportsCar(make, model, turbocharged) {
    Car.call(this, make, model);
    this.turbocharged = turbocharged;
  }
  
  // actual inheritance logic
  SportsCar.prototype = Object.create(Car.prototype);
  SportsCar.prototype.constructor = SportsCar;
  
  // overriding the start method
  SportsCar.prototype.start = function() {
    console.log('VROOOOM');
  }
  
  // Now testing the classes
  var car = new Car('Nissan', 'Sunny');
  car.start(); // vroom
  console.log(car.make); // Nissan
  
  var sportsCar = new SportsCar('Subaru', 'BRZ', true);
  sportsCar.start(); // VROOOOM
  console.log(car.turbocharged); // true */

  class Car {
    constructor(make, model) {
      this.make = make;
      this.model = model;
    }
    
    start() {
      console.log('vroom');
    }
    
    toString() {
      console.log(`Car - ${this.make} - ${this.model}`);
    }
  }
  
  class SportsCar extends Car {
    constructor(make, model, turbocharged) {
      super(make, model);
      this.turbocharged = turbocharged;
    }
    
    start() {
      console.log('VROOOOM');
    }
  }
  
  
  // Actual usage remains the same
  var car = new Car('Nissan', 'Sunny');
  car.start(); // vroom
  console.log(car.make); // Nissan
  
  var sportsCar = new SportsCar('Subaru', 'BRZ', true);
  sportsCar.start(); // VROOOOM
  console.log(sportsCar.toString()); // Car - Subaru - BRZ

  // WHY NOT
  // -- Binding issues especially if you try  to pass your class  method
  // as a callback to an external routine (React)
  // -- Performance issues - classes implementation, they are notoriously 
  // difficult to optimize at runtime
  // -- Private variables - modifiers are not existent in JS
  // -- Strict hierarchies - classes introduce a straight top to bottom order
  // which harder to implement
  // Because the React team tells you not to. 
  // While they did not explicitly deprecate the class-based 
  // components yet, they are likely to in the near future.

  // All of these issues can be mitigated with JS
  //  objects and prototype delegation.
