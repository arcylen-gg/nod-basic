
const objSales = { 
    Aigoo : 50, 
    Cornietta : 90, 
    Jaddy : 80, 
    Jaccy : 30, 
    Hamtaro : 70
}

/**
 Descriptor Groups
 DATA               ACCESS
 value              get -- retrieve the property value (default is `undefined`)
 writable           set -- updating the value (default `true`)
 configurable       configurable -- allowed to change (default `true`)
 enumerable         enumerable -- if it will show when enumerating 
                        the object (default `true`)
 */

Object.defineProperty(objSales, 'Hamtaro', {
    value: 55,
    writable: true,
    enumerable: false,
    configurable: true,
})

for(let obj in objSales) {
    console.log(obj)
}

console.log(Object.getOwnPropertyDescriptors(objSales))