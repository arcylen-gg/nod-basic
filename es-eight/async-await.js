// Async await

const fetch = require('node-fetch')

async function fetchAvatarUrl(userId) {
    // will wait for await before reading the next line
    const response = await fetch(`https://catappapi.herokuapp.com/users/${userId}`)
    const data = await response.json() // still a promise
    console.log(data.imageUrl)
    return data.imageUrl 


    /* return fetch(`https://catappapi.herokuapp.com/users/${userId}`)
        .then(response => response.json())
        .then(data => data.imageUrl) */
}

const result = fetchAvatarUrl(123)
console.log(result)

// Inside a function marked as `async`, you are allowede to place the `await`
// keyword in front of an expression that returns a promise. 
// When you do execution of the `async` function is paused 
// until the promise is resolved

// Why
// -- the idea of behind the async await code is to be able to write
// asyncronous code thats's flows like syncronous code


async function fetchCatAvatars(userId) {
    const response = await fetch(`https://catappapi.herokuapp.com/users/${userId}`)
    const user = await response.json()

    return await Promise.all(user.cats.map(async function(catId) {
        const responseCat = await fetch(`https://catappapi.herokuapp.com/cats/${catId}`)
        const catData = await responseCat.json()
        console.log(catData.imageUrl)
        return catData.imageUrl
    }))
/*
    const catImageUrls = []
    for (const catId of user.cats) {
        const responseCat = await fetch(`https://catappapi.herokuapp.com/cats/${catId}`)
        const catData = await responseCat.json()
        catImageUrls.push(catData.imageUrl)
        console.log(catData.imageUrl)
    }
    return catImageUrls
*/
/* 
    return fetch(`https://catappapi.herokuapp.com/users/${userId}`)
        .then(response => response.json())
        .then(user => {
            console.log(user)
            const promises = user.cats.map(catId => {
                fetch(`https://catappapi.herokuapp.com/cats/${catId}`)
                    .then(response => response.json())
                    .then(catData => catData.imageUrl)
            })
            return Promise.all(promises)
        })
*/
}

// await cannot work with multiple promises,
// its only works with single promises

const catResult = fetchCatAvatars(123)
console.log(catResult)