// Array prototype includes

const pets = ['Aigoo', 'Cornietta', 'Hamtaro']

// Case sensitive
console.log(pets.includes('Aigoo')) // true

console.log(pets.includes('aigoo')) // false